module.exports = {
  devServer:{
    port:8082,
    host:'localhost',
    open:true
  },
  configureWebpack: {
    devtool: 'source-map'
  }
}
