# ZoomSense Icebreaker

This repo contains a student Master's project investigating how to support icebreaker activities in Zoom by augmenting ZoomSense with chat-bot functionality. Its a basic VueJS application with minimal configuration.

## ZoomSense Setup

Create a file called `.env` with the following variables which point to a valid ZoomSense deployment:

```
VUE_APP_API_KEY=
VUE_APP_AUTH_DOMAIN=
VUE_APP_DATABASE_URL=
VUE_APP_PROJECT_ID=
VUE_APP_STORAGE_BUCKET=
VUE_APP_FUNCTION_URL=
VUE_APP_MESSAGING_SENDER_ID=
VUE_APP_APP_ID=
```

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```
