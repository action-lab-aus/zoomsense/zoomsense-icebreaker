import Vue from 'vue'
import VueRouter from 'vue-router'
import { auth } from '../firebase'
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import( '../views/Login.vue'),
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import( '../views/Home/Index/Index.vue'),
    meta: {
      requiresAuth: true
    },
    children: [
      {
        path: '/index',
        name: 'Index',
        component: () => import( '../views/Home/Index/Index.vue'),
        meta: {
          requiresAuth: true
        },
      },
    ]
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  const currentUser = auth.currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (!requiresAuth)
    return next();

  if (currentUser && to.path.startsWith('/login'))
  {
    return next('home');
  }

  if (requiresAuth && !currentUser){
    next('login')
  }
  else if (currentUser && requiresAuth)
  {
    next();
  }
  else next();
});

export default router

