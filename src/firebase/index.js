import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";

// firebase init - add your own config here
const firebaseConfig = {
  apiKey: process.env.VUE_APP_API_KEY,
  authDomain: process.env.VUE_APP_AUTH_DOMAIN,
  databaseURL: process.env.VUE_APP_DATABASE_URL,
  projectId: process.env.VUE_APP_PROJECT_ID,
  storageBucket: process.env.VUE_APP_STORAGE_BUCKET,
  functions_url: process.env.VUE_APP_FUNCTION_URL,
  messagingSenderId: process.env.VUE_APP_MESSAGING_SENDER_ID,
  appId: process.env.VUE_APP_APP_ID,
};
const fb = firebase.initializeApp(firebaseConfig);

// utils
const db = fb.database();
const auth = fb.auth();

// collection references
const zoomdata = db.ref("data");

// export utils/refs
export { db, auth, zoomdata };
